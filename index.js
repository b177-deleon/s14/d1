// JavaScript Comments
// This is a single line comment [ctrl + /]
/*
	This is a multi line comment [ctrl + shft + /]
*/

// Syntac and Statements
/*
	- Statements in programming are instructions that we tell the computer to perform. It usually ends with semicolon (;)
	- A syntax in programming is the set of rules that describes how statements must be constructed

*/

console.log("Hello, world");

// Variables
// It is used to contain data
// Declaring a variable

/* Syntax
	let/const variableName;
*/

let myVariable;

console.log(myVariable);

// console.log(hello);

// let hello;

// Initializing Variables
/* Syntax
	let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
const pi = 3.1416;

// Reassigning variable values
/* Syntax
	variableName = newValue;
*/

productName = 'Laptop';
console.log(productName);

// interest = 4.489;
// console.log(interest)

// Declaring a variable before initializing a value
let supplier;

supplier = "John Smith Tradings"
console.log(supplier);

// Multiple variable declarations
let productCode = 'DC017', productBrand = 'Dell'
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
/*const let = "Hello";
console.log(let);*/

// Data Types

/* STrings
	- series of characters that create a word, phrase, sentence or anything related to creating text
*/

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress)

let greeting = "I live in the " + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers 
// Integers/Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and numbers
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
/* Syntax
	let/const arrayName = [elementA, elementB, elementC, ...];

	let grade1 = 98.7;
	let grade2 = 92.1;

*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects or items
/* Syntax
	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/
let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

// Null
// It is used intentionally to express the absence of a value in a variable.
let spouse = null;
let myString = ' ';
let myNumber = 0;
console.log(spouse);

// Functions
/* Syntax
	function functionName(){
		line/block of code to be executed by the function;
	};
*/

/*function printName(){
	console.log("My name is John");
};

printName();
*/

function printName(name){
	console.log("My name is " + name);
};

printName("John");

// "My favorite animal is a "

function printAnimal(name){
	console.log("My favorite animal is a " + name);
};

printAnimal ("dog");

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Creating another function with multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
};

createFullName('Juan', 'Diaz', 'Dela Cruz');
createFullName('Juan', "Dela Cruz");

createFullName('Juan', 'Diaz', 'Dela Cruz', 'Hello');

// Creating a function using "statement" statement
// "return" statement allows the output of a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName
	console.log("This message will not be printed");
}

console.log(returnFullName('Maria', 'Clara', 'Ibarra'));

let completeName = returnFullName('Maria', 'Clara', 'Ibarra');
console.log(completeName);
